"Phrases"
{
	"FPS_KickMsg"
	{
		"#format"	"{1:i}"
		"en"		"{darkred}You will be kicked in 10 seconds. Please set fps_max between 100 and 300! (Current setting: fps_max {1})"
	}
	"MapTopFail"
	{
		"en"		"{grey}Usage: {lime}!maptop <mapname>"
	}
	"SkillGroup"
	{
		"#format"	"{1:s}"
		"en"		"{grey}You are now ranked as {1}"
	}	
	"EarnedPoints"
	{
		"#format"	"{1:s},{2:i},{3:i}"
		"en"		"{darkblue}{1} {grey}has earned {red}{2} {grey}points [{red}{3} {grey}Total]"
	}
	"BotAlreadyCreated"
	{
		"en"		"{grey}Replay bot already spawned."
	}
	"NoPlayerTop"
	{
		"en"		"{grey}No players found."
	}
	"PrUpdateFinished"
	{
		"en"		"{grey}Recalculation finished!"
	}
	"Top100Refreshed"
	{
		"en"		"{grey}Top 100 Players recalculated!"
	}
	"PlayerHasNoMapRecords"
	{
		"#format"	"{1:s}"
		"en"		"{darkblue}{1} {grey}doesn't have local map times."
	}
	"NoTopRecords"
	{
		"#format"	"{1:s}"
		"en"		"{grey}No Local times found on {lime}{1}"
	}
	"NoMapRecords"
	{
		"#format"	"{1:s}"
		"en"		"{grey}No Local map times found on {lime}{1}"
	}
	"AdminSetButton"
	{
		"en"		"{grey}Failed! You must be alive to set a new button."
	}
	"PositionRestored"
	{
		"en"		" \n<font size='23' color='#00CC00'><b>Time and position restored</b></font>\n"
	}
	"menu_rank_no_colors_keep_spaces"
	{
		"en"		"Top 5 Challengers\n#   W/L P.-Ratio    Player (W/L ratio)"
	}
	"Rc_PlayerRankStart"
	{
		"en"        "{grey}Refreshing profile data.."
	}
	"Rc_in_progress"
	{
		"en"        "{grey}Recalculation in progress. Please wait!"
	}
	"Rc_PlayerRankFinished"
	{
		"#format"   "{1:i}"
		"en"        "Profile refreshed. [{1}]"
	}
}