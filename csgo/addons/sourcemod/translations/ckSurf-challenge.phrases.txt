"Phrases"
{
	"ChallengeStarted1"
	{
		"en"		"{grey}If you want to surrender just type {lime}!surrender"
	}
	"ChallengeStarted2"
	{
		"en"		"{grey}Both of you can type {lime}!abort {grey}in chat to cancel the race"
	}
	"ChallengeStarted3"
	{
		"en"		"{grey}Have Fun!!"
	}
	"ChallengeCountdown"
	{
		"#format"	"{1:d}"
		"en"		"{red}{1}s"
	}
	"ChallengeRequestExpired"
	{
		"en"		"{grey}Challenge request expired!"
	}
	"ChallengeWon"
	{
		"en"		"{grey}Challenge won! (your opponent has left the server)"
	}
	"ChallengeAborted"
	{
		"#format"	"{1:s}"
		"en"		"{grey}Challenge versus {darkblue}{1} {grey}aborted."
	}
	"no_challenges_found"
	{
		"#format" 	"{1:s}"
		"en"		"{grey}No challenges against {darkblue}{1} {grey}found"
	}
}