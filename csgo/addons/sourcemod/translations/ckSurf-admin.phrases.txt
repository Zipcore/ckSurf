"Phrases"
{
	"ZoneVisAll"
	{
		"en"		"{grey}The zone is visible for ALL."
	}
	"ZoneVisT"
	{
		"en"		"{grey}The zone is visible for Terrorists."
	}
	"ZoneVisCT"
	{
		"en"		"{grey}The zone is visible for Counter-Terrorists."
	}
	"ZoneVisInv"
	{
		"en"		"{grey}The zone is invisible."
	}
}