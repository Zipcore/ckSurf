
<h1>ckSurf 2.0 </h1>
<p><strong>Updating instructions from 1.17 -> 1.18: https://forums.alliedmods.net/showpost.php?p=2371063&postcount=841</strong><p>
<p><strong>Updating instructions from 1.18, 1.19 -> 2.0: download the new skillgroups.cfg and custom_chat_titles, replace the old one in addons/sourcemod/configs. </br>
If you want to keep your old titles then you'll have to manually copy over the old ones and make <b>SURE</b> that the colors are supported</strong><p>
<p><strong>Only works on SourceMod 1.8+</strong><p>
<p>The original developer(s) quit their work, the latest (Jonitaikaponi) has gone as far as deleting the repo. We (Zipcore and Deadhuntz) will keep it going</p>